package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import net.techu.hackathon.grupo8.models.TipoTarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TipoTarjetaRepositoryImpl implements TipoTarjetaRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public TipoTarjetaRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<TipoTarjeta> findAll() {
        List<TipoTarjeta> tipoTarjetas = this.mongoOperations.find(new Query(), TipoTarjeta.class);
        return tipoTarjetas;
    }

    @Override
    public TipoTarjeta findOne(String id) {
        TipoTarjeta encontrado = this.mongoOperations.findOne(new Query(Criteria.where("cardtype_id").is(id)),TipoTarjeta.class);
        return encontrado;
    }

    @Override
    public TipoTarjeta saveTipoTarjeta(TipoTarjeta tipoTarjeta) {
        this.mongoOperations.save(tipoTarjeta);
        return findOne(tipoTarjeta.getCardtype_id());
    }

    @Override
    public void updateTipoTarjeta(TipoTarjeta tipoTarjeta) {
        this.mongoOperations.save(tipoTarjeta);
    }

    @Override
    public void deleteTipoTarjeta(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("cardtype_id").is(id)), TipoTarjeta.class);
    }
}
