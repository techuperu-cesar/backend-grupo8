package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;

import java.util.List;

public interface CategoriaTarjetaRepository {
    List<CategoriaTarjeta> findAll();
    public CategoriaTarjeta findOne(String id);
    public CategoriaTarjeta saveCategoriaTarjeta(CategoriaTarjeta client);
    public void updateCategoriaTarjeta(CategoriaTarjeta client);
    public void deleteCategoriaTarjeta(String id);
}
