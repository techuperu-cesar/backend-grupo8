package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import net.techu.hackathon.grupo8.models.TipoTarjeta;
import net.techu.hackathon.grupo8.repository.CategoriaTarjetaRepository;
import net.techu.hackathon.grupo8.repository.TipoTarjetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("tipoTarjetaService")
@Transactional
public class TipoTarjetaServiceImpl implements TipoTarjetaService {
    private TipoTarjetaRepository tipoTarjetaRepository;

    @Autowired
    public TipoTarjetaServiceImpl(TipoTarjetaRepository tipoTarjetaRepository)
    {
        this.tipoTarjetaRepository = tipoTarjetaRepository;
    }

    @Override
    public List<TipoTarjeta> findAll() {
        return tipoTarjetaRepository.findAll();
    }

    @Override
    public TipoTarjeta findOne(String id) {
        return tipoTarjetaRepository.findOne(id);
    }

    @Override
    public TipoTarjeta saveTipoTarjeta(TipoTarjeta veh) { return tipoTarjetaRepository.saveTipoTarjeta(veh); }

    @Override
    public void updateTipoTarjeta(TipoTarjeta veh) { tipoTarjetaRepository.updateTipoTarjeta(veh); }

    @Override
    public void deleteTipoTarjeta(String id) { tipoTarjetaRepository.deleteTipoTarjeta(id); }
}
