package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.Cliente;

import java.util.List;

public interface ClienteService {
    List<Cliente> findAll();
    public Cliente findOne(String id);
    public Cliente saveCliente(Cliente veh);
    public void updateCliente(Cliente veh);
    public void deleteCliente(String id);
}
