package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.Tarjeta;

import java.util.List;

public interface TarjetaService {
    List<Tarjeta> findAll() ;
    public List<Tarjeta> findTarjetasCliente(Integer customer_id);
}
