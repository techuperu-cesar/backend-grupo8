package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;

import java.util.List;

public interface CategoriaTarjetaService {
    List<CategoriaTarjeta> findAll();
    public CategoriaTarjeta findOne(String id);
    public CategoriaTarjeta saveCategoriaTarjeta(CategoriaTarjeta veh);
    public void updateCategoriaTarjeta(CategoriaTarjeta veh);
    public void deleteCategoriaTarjeta(String id);
}
