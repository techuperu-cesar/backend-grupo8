package net.techu.hackathon.grupo8.controllers;

import net.techu.hackathon.grupo8.models.ClienteTarjetaModel;
import net.techu.hackathon.grupo8.models.TarjetaModel;
import net.techu.hackathon.grupo8.service.ClienteTarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton/v2")
public class ClienteTarjetaController {
    @Autowired
    ClienteTarjetaService clienteTarjetaService;

    @GetMapping("/clientes")
    public List<ClienteTarjetaModel> getClientesTarjetas() {
        return clienteTarjetaService.findAll();
    }

    @GetMapping("/clientes/{id}")
    public Optional<ClienteTarjetaModel> getClienteId(@PathVariable String id) {
        return clienteTarjetaService.findById(id);
    }

    @PostMapping("/clientes")
    public ClienteTarjetaModel postClienteTarjeta(@RequestBody ClienteTarjetaModel newCliente) {
        clienteTarjetaService.save(newCliente);
        return newCliente;
    }

    @PutMapping("/clientes")
    public void putClienteTarjeta(@RequestBody ClienteTarjetaModel clienteToUpdate) {
        clienteTarjetaService.save(clienteToUpdate);
    }

    @PutMapping("/clientes/{cliente}")
    public ClienteTarjetaModel insertTarjetaCliente(@PathVariable String cliente, @RequestBody TarjetaModel tarjetaModels) {
        return clienteTarjetaService.insertTarjeta(cliente, tarjetaModels);
    }

    @DeleteMapping("/clientes")
    public boolean deleteCliente(@RequestBody ClienteTarjetaModel clienteToDelete) {
        return clienteTarjetaService.delete(clienteToDelete);
    }


}
