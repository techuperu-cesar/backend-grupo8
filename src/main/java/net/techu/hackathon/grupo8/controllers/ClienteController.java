package net.techu.hackathon.grupo8.controllers;

import net.techu.hackathon.grupo8.models.Cliente;
import net.techu.hackathon.grupo8.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton/v1")
public class ClienteController {
    private final ClienteService clienteService;
    private Cliente cliente;

    @Autowired
    public ClienteController(ClienteService clienteService)
    {
        this.clienteService = clienteService;
    }

    @GetMapping("/clientes")
    public ResponseEntity<List<Cliente>> clientes()
    {
        return ResponseEntity.ok(clienteService.findAll());
    }

    @PostMapping("/clientes")
    public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente)
    {
        return ResponseEntity.ok(clienteService.saveCliente(cliente));
    }
}
