package net.techu.hackathon.grupo8.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tipostarjeta")
@JsonPropertyOrder({"id","cardtype_id", "description"})
public class TipoTarjeta {
    @Id
    @NotNull
    private String id;
    @NotNull
    private String cardtype_id;
    @NotNull
    private String description;

    public TipoTarjeta() {}

    public TipoTarjeta(String id, String cardtype_id, String description)
    {
        this.setId(id);
        this.setCardtype_id(cardtype_id);
        this.setDescription(description);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardtype_id() {
        return cardtype_id;
    }

    public void setCardtype_id(String cardtype_id) {
        this.cardtype_id = cardtype_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
