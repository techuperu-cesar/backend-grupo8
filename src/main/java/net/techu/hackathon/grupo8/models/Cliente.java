package net.techu.hackathon.grupo8.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "clientes")
@JsonPropertyOrder({"id","customer_id", "last_name", "first_name", "phone_number", "address_desc"})

public class Cliente {
    @Id
    @NotNull
    private String id;
    @NotNull
    private String customer_id;
    @NotNull
    private String last_name;
    @NotNull
    private String first_name;
    @NotNull
    private String phone_number;
    @NotNull
    private String address_desc;

    public Cliente() {}

    public Cliente(String id, String customer_id, String last_name, String first_name, String phone_number, String address_desc)
    {
        this.setId(id);
        this.setCustomer_id(customer_id);
        this.setLast_name(last_name);
        this.setFirst_name(first_name);
        this.setPhone_number(phone_number);
        this.setAddress_desc(address_desc);
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAddress_desc() {
        return address_desc;
    }

    public void setAddress_desc(String address_desc) {
        this.address_desc = address_desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
